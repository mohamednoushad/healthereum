import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Component, OnInit, OnChanges } from '@angular/core';
import { NavbarService } from './shared/sevices/navbar/navbar.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  
  title = 'healthereum';
  isLoggedIn$ : Observable<boolean>;
  user: string;
  constructor(private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.isLoggedIn$ = this.authService.isLoggedIn;
    
  }

  logout() {
    this.authService.logout();
  }
}
