import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { User } from './../services/user';
import { Router } from '@angular/router';
import { Component, OnInit } from "@angular/core";
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';


@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"]
})
export class SignupComponent implements OnInit {

  providers: Array<any>;
  provider: string;
  user:User =  <User>new Object();
  showOtpform: boolean = false;
  otp: number = null;
  signupForm2 = false;
  loading:boolean = false;
  constructor(private router: Router, 
        private authService: AuthenticationService,
        private toastr: ToastrService) {


  }

  ngOnInit() {
    this.getProviders();
  }

  getProviders(): void {
    this.authService.getProviders()
      .subscribe(response => {
        this.providers = response['providers'];
        this.provider = this.providers[0];
      },error => {
        this.loading = false;
        this.toastr.error("Unable To Fetch Providers", "Please try again after sometime.");
        console.log(error);
      });
  }



  nextSignup2() {
    this.signupForm2 = true;
  }
  nextSignup1() {
    this.signupForm2 = false;
  }

  searchUser() {

    this.user.provider = this.provider;
    this.loading = true;
    this.authService.searchUser(this.user).subscribe(response => {
      this.loading = false;

      if(response.total>0){

        this.user.user = "patient";
        this.user.existingEmrUser = true;
        this.user.existingEmrPatientId = response.entry[0].resource.id;
        this.user.firstName = response.entry[0].resource.name[0].given[0];
        this.user.familyName = response.entry[0].resource.name[0].family;
        this.user.dob =  response.entry[0].resource.birthDate;
        this.user.gender =response.entry[0].resource.gender.toLowerCase();
        this.user.addressLine1 = response.entry[0].resource.address[0].line[0];
        this.user.addressLine2 = response.entry[0].resource.address[0].line[1];
        this.user.city = response.entry[0].resource.address[0].city;
        this.user.state = response.entry[0].resource.address[0].state;
        this.user.areaCode = response.entry[0].resource.address[0].postalCode;
        this.user.country = response.entry[0].resource.address[0].country;
        
      }else{

        this.toastr.error("User Not Present in Emr", "Please check the EMR number.");

      }
      
    },
    error => {
      this.loading = false;
      this.toastr.error("Error Searching the User", "Something went wrong. Please try again after sometime.");
      console.log(error);

    })

  }

  complete() {
    this.user.isRegistered = false;
    this.user.provider = this.provider;
    console.log(this.user);
    this.loading = true;
    this.authService.generateOTP(this.user).subscribe(response => {
      if (response) {
        this.loading =false;
        this.toastr.success("Success!!!", "OTP generated successfully");
        console.log(response);
        this.showOtpform=true;
      }
    },
      error => {
        this.loading = false;
        this.toastr.error("OTP generation failed", "Something went wrong. Please try again after sometime.");
       console.log(error);
      });
  }
  signup () {
    this.user.otp = this.otp;
    this.loading = true
    this.authService.signup(this.user)
      .subscribe(response => {
        if (response) {
          console.log(response);
          this.loading = false;
          this.toastr.success("Success!!!", "Your account has been successfully created");
          this.router.navigate(['/signin']);
        }
      },
        error => {
          this.loading = false;
          this.toastr.error("Registration failed!!!", "Something went wrong. Please try again after sometime.");
         console.log(error);
        });
  }
}
