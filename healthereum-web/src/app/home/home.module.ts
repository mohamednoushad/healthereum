//import { DashboardModule } from './dashboard/dashboard.module';
import { AngularMaterialModule } from './../angular-material/angular-material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { AppSideNavComponent } from './app-side-nav/app-side-nav.component'; 
import { HomeRoutingModule } from './home-routing.module';
import { ActivityLogComponent } from './dashboard/activity-log/activity-log.component';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { DoctorComponent } from './dashboard/doctor/doctor.component';
import { DelegateComponent } from './dashboard/delegate/delegate.component';
import { DetailsComponent } from './dashboard/details/details.component';
import { ProfileComponent } from './dashboard/profile/profile.component';

@NgModule({
  declarations: [HomeComponent, ActivityLogComponent, DoctorComponent, DelegateComponent, DetailsComponent, ProfileComponent],
  imports: [
    CommonModule, AngularMaterialModule, RouterModule, HomeRoutingModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.rectangleBounce,
        backdropBackgroundColour: 'rgba(0,0,0,0.8)'
    })
  ]
})
export class HomeModule { }
