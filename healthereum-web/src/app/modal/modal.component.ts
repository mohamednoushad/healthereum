import { Component, OnInit, Inject, Output, EventEmitter } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { DelegateComponent } from '../home/dashboard/delegate/delegate.component';

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"]
})
export class ModalComponent implements OnInit {

  @Output() onDelegate = new EventEmitter<any>(true);
  @Output() onDoctorDelegation = new EventEmitter<any>(true);
  @Output() onPatientDataFetch = new EventEmitter<any>(true);
  @Output() onFileUpload = new EventEmitter<any>(true);
  nominee: string;
  doctor: any;
  data: any;
  provider: any;
  myAngularxQrCode: string;
  file: any;
  constructor(public delegateDialogRef: MatDialogRef<DelegateComponent>,
    @Inject(MAT_DIALOG_DATA) dialogData) {
      this.data = dialogData;
      this.myAngularxQrCode = this.data.address;
  }

  ngOnInit() { }

  delegate() {
    this.onDelegate.emit(this.nominee);
  }

  delegateDoctor(): void {
    this.onDoctorDelegation.emit(this.doctor);
  }

  getDemographics(){
    const provider = this.data.providers && (this.data.providers.length > 1) ? this.provider : this.data.providers[0];
    this.onPatientDataFetch.emit({provider, isDownload: this.data.isDownload});
  }

  onUpload (file:FileList) {
    this.file = file.item(0);
  }
  uploadFile() {
    this.onFileUpload.emit(this.file);
  }

}
