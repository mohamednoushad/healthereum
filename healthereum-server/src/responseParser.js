function parsePrescriptionResponse(openmrsReponse){
    if(!openmrsReponse || !openmrsReponse.total){
        return {count: 0, prescriptions: []};
    }

    let prescriptions = [];
    if(openmrsReponse.entry){
        for(let entry of openmrsReponse.entry){
            let requesterName = (entry.resource.requester.agent.display).split("(");
            let recorderName = (entry.resource.recorder.display).split("(");
            let prescription = {
                status: entry.resource.status,
                authoredOn: entry.resource.authoredOn,
                requester: {id: entry.resource.requester.agent.id, name: requesterName[0]},
                recorder: {id: entry.resource.recorder.id, name: recorderName[0]},
                dispenseRequest: entry.resource.dispenseRequest
            };
            if(entry.resource.medicationCodeableConcept.coding && entry.resource.medicationCodeableConcept.coding.length){
                prescription.drugName = (entry.resource.medicationCodeableConcept.coding[entry.resource.medicationCodeableConcept.coding.length - 1]).display;
            }
            prescription.dosageInstruction =[];
            for(let dosageInstruction of entry.resource.dosageInstruction){
                let dosage = {
                    timing: dosageInstruction.timing? dosageInstruction.timing.code.text : null,
                    route: dosageInstruction.route? dosageInstruction.route.text : null,
                    doseQuantity: dosageInstruction.doseQuantity,
                    maxDosePerAdministration: dosageInstruction.maxDosePerAdministration,
                    dosageInstructionFreeText: dosageInstruction.text
                }
                prescription.dosageInstruction.push(dosage);
            }
            prescriptions.push(prescription);
        }
    }

    return {count:prescriptions.length,prescriptions: prescriptions};
}


module.exports = {
    parsePrescriptionResponse
}
