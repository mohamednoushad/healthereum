const uuidv1 = require('uuid/v1');

function uuid() {
    return uuidv1();
}

module.exports = {
    uuid
};
