import transaction from "./transact";
import DBService from '../services/dbservice';
import keythereum from '../keystore/keythereum';
import ownershipTransaction from "../ownership/transact";
import { decrypt } from '../helper';
import userRepo from "../user/userRepo";

module.exports = {
    add: async (data) => {
           try {
            const { nominee_id, user_id } = data;

            const currentUser = await userRepo.getUserWithSSN(user_id);
            if(!currentUser){
                throw new Error("User not found");
            }
            const nominee = await userRepo.getUserWithSSN(nominee_id);
            if(!nominee){
                throw new Error("Nominee not found");
            }

            let userCypherResult = await userRepo.getUserCypherText(currentUser.globalID);
            const user_password = decrypt(userCypherResult.cipher).password;

            const user_privatekey = await keythereum.getPrivateKey('0x' + currentUser.account, user_password);
            const accounts = await transaction.addNominee('0x' + nominee.account, '0x' + currentUser.account, user_privatekey);
            if (accounts && accounts.length) {
                return await userRepo.getUsersWithAccounts(accounts);
            }

        }
        catch (err) {
            throw err;
        }
    },

    update: () => {
        return new Promise((resolve, reject) => {

        })
    },

    revoke: async (data) => {
        try {
            const { nominee_id, user_id } = data;

            const currentUser = await userRepo.getUserWithSSN(user_id);
            if(!currentUser){
                throw new Error("User not found");
            }
            const nominee = await userRepo.getUserWithSSN(nominee_id);
            if(!nominee){
                throw new Error("Nominee not found");
            }

            let userCypherResult = await userRepo.getUserCypherText(currentUser.globalID);
            const user_password = decrypt(userCypherResult.cipher).password;

            const getUserPrivatekey = keythereum.getPrivateKey('0x' + currentUser.account, user_password);
            const nominees = await transaction.getNominees('0x' + currentUser.account);
            const nominators = await transaction.getNominators('0x' + nominee.account);

            const nomineeIndex = nominees.findIndex(address => '0x' + nominee.account.toLowerCase() === address.toLowerCase());
            const nominatorIndex = nominators.findIndex(address => '0x' + currentUser.account.toLowerCase() === address.toLowerCase());

            let user_privatekey = await getUserPrivatekey;
            const params = ['0x' + nominee.account, '0x' + currentUser.account, nomineeIndex, nominatorIndex];
            const accounts = await transaction.revokeNomination(params, user_privatekey);

            if (accounts && accounts.length) {
                return await userRepo.getUsersWithAccounts(accounts);
            }
        }
        catch (err) {
            throw err;
        }
    },

    getNominees: async (data) => {
        try {
            const { userId } = data;
            const user = await userRepo.getUserWithSSN(userId);
            if(!user){
                throw new Error("User not found");
            }

            let accounts = await transaction.getNominees('0x' + user.account);
            if (accounts && accounts.length) {
                    return await userRepo.getUsersWithAccounts(accounts);
            }
        }
        catch (err) {
            throw err;
        }
    },

    getNominators: async (data) => {
        try {
            const { userId } = data;
            const user = await userRepo.getUserWithSSN(userId);
            if(!user){
                throw new Error("User not found");
            }

            let accounts = await transaction.getNominators('0x' + user.account);
            if (accounts && accounts.length) {
                return await userRepo.getUsersWithAccounts(accounts);
            }
        }
        catch (err) {
            throw err;
        }
    },

    grantAccess: async (data) => {
        try {
            const { nominator_id, user_id, doctorAddress, grantAccess } = data;

            const nominator = await userRepo.getUserWithSSN(nominator_id);
            if(!nominator){
                throw new Error("Nominator not found");
            }
            const currentUser = await userRepo.getUserWithSSN(user_id);
            if(!currentUser){
                throw new Error("User not found");
            }

            const userNominators = await transaction.getNominators('0x' + currentUser.account);

            const isUserNominated = userNominators.find(nominatorAddress => '0x' + nominator.account.toLowerCase() === nominatorAddress.toLowerCase());
            if (isUserNominated) {
                let userCypherResult = await userRepo.getUserCypherText(currentUser.globalID);
                const user_password = decrypt(userCypherResult.cipher).password;

                const user_privatekey = await keythereum.getPrivateKey('0x' + currentUser.account, user_password);
                const shareNominatorAddress = await ownershipTransaction.manage('0x' + nominator.account, doctorAddress, nominator.name, user_privatekey, grantAccess);
                return shareNominatorAddress;
            }
            else {
                throw {
                    status: "404",
                    message: "Invalid user"
                }
            }
        }
        catch (e) {
            throw e;
        }
    }

}
