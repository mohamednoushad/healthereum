module.exports = {
    GET_UNREAD_NOTIFICATIONS_FOR_USER: `SELECT Id AS id, Message AS message, IsRead AS isRead, CreatedOn AS createdOn FROM Notifications WHERE UserId=? AND IsRead=false ORDER BY CreatedOn DESC`,
    GET_UNREAD_NOTIFICATIONS_COUNT_FOR_USER: `SELECT COUNT(*) AS count FROM Notifications WHERE UserId=? AND IsRead=false`,
    markNotificationsAsReadQuery: markNotificationsAsReadQuery,
    CREATE_NOTIFICATION_QUERY: `INSERT INTO Notifications (Id, Message, UserId, IsRead, CreatedOn) VALUES (?, ?, ?, ?, ?);`
};

function markNotificationsAsReadQuery(notifications) {
    let notificationIds = '';

    notificationIds += ' (';
    for (let item of notifications) {
        notificationIds += `'${item}'`;
        (notifications.indexOf(item) < notifications.length - 1) ?  notificationIds += ',' :  notificationIds += ')';
    }

    return `UPDATE Notifications SET IsRead=true WHERE Id IN ${notificationIds}`;
}
