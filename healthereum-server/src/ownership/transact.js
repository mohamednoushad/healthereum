import transaction from '../transaction';
import config from '../config'
import PATH from 'path';
const notificationRepo = require(PATH.resolve('src/notifications')).repo;
const abi = config.abi;

module.exports = {
    manage: async (userAddress, doctorAddress, userName, userPrivateKey, grantAccess) => {
        try {
            let params,
                patients,
                doctors,
                tx;
            const patientsTx = transaction.callBlockchainTransaction("getGivenAccesslist", [doctorAddress]);
            const doctorsTx = transaction.callBlockchainTransaction("getPatientSharedList", [userAddress]);
            patients = await patientsTx;
            doctors = await doctorsTx;
            patients = patients && patients.data ? patients.data : [];
            doctors = doctors && doctors.data ? doctors.data : [];
            const userAddressIndex = patients.findIndex(address => address.toLowerCase() === userAddress.toLowerCase());
            const doctorAddressIndex = doctors.findIndex(address => address.toLowerCase() === doctorAddress.toLowerCase());
            if (grantAccess) {
                if (userAddressIndex === -1 && doctorAddressIndex === -1) {
                    params = [userAddress, doctorAddress];
                    tx = await transaction.callBlockchainTransaction("giveAccessToDoctor", params, userPrivateKey);
                }
                else {
                    return {
                        status: 401,
                        message: "Patient data already shared with doctor"
                    }
                }
            }
            else {
                if (userAddressIndex !== -1 && doctorAddressIndex !== -1) {
                    params = [userAddress, doctorAddressIndex, userAddressIndex, doctorAddress];
                    tx = await transaction.callBlockchainTransaction("revokeAccessFromDoctor", params, userPrivateKey);
                }
                else {
                    return {
                        status: 401,
                        message: "Patient already revoked access for the doctor"
                    }
                }
            }
            if (tx && tx.status === 200) {
                await notificationRepo.createNotification({
                    patientDetails:
                    {
                        patientPublicKey: userAddress,
                        patientName: userName
                    },
                    doctorDetails:
                    {
                        doctorPublicKey: doctorAddress
                    },
                    isAccessGranted: grantAccess
                });
                return tx;
            }
            else {
                throw {
                    status: 500,
                    message: "Error occured"
                }
            }
        }
        catch (e) {
            throw { status: 500, error:e };
        }
    }
}
