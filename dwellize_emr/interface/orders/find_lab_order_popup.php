<?php
/**
 * Script to pick a procedure order type from the compendium.
 *
 * Copyright (C) 2013 Rod Roark <rod@sunsetsystems.com>
 *
 * LICENSE: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://opensource.org/licenses/gpl-license.php>.
 *
 * @package   OpenEMR
 * @author    Rod Roark <rod@sunsetsystems.com>
 * @author    Jerry Padgett <sjpadgett@gmail.com>
 */


require_once("../globals.php");

use OpenEMR\Core\Header;

$modality = $_GET['modality'];
$order = 0 + $_GET['order'];
$labid = 0 + $_GET['labid'];

//////////////////////////////////////////////////////////////////////
// The form was submitted with the selected code type.
if (isset($_GET['typeid'])) {
    $grporders = array();
    $typeid = $_GET['typeid'];
    $name = '';
    if ($typeid) {
        $query = "SELECT * FROM (SELECT DISTINCT sct2_description.id,conceptId, term
                    FROM sct2_relationship JOIN sct2_description ON sct2_description.conceptId= sct2_relationship.sourceId 
                    WHERE sct2_description.active = 1 AND destinationId IN 
                    (SELECT sourceId FROM sct2_relationship WHERE sct2_description.active = 1 AND destinationId = 15220000)) as res WHERE id = '".$typeid."'";
        $ptrow = sqlQuery($query);
        $name = addslashes($ptrow['term']);
        $codes = addslashes($ptrow['conceptId']);        
        $typeid = $typeid;
    }
    ?>   
    <script type="text/javascript" src="<?php echo $webroot ?>/interface/main/tabs/js/include_opener.js"></script>
    <script language="JavaScript">
        if (opener.closed) {
            alert('<?php xl('The destination form was closed; I cannot act on your selection.', 'e'); ?>');
        }
        else {
            <?php          
            $i = 0;
            $t = 0;
            do {
                if (!isset($grporders[$i]['procedure_type_id'])) {
                    echo "opener.set_proc_type('$typeid', '$name', '$codes');\n";
                } else {
                    $t = count($grporders) - $i;
                    $typeid = $grporders[$i]['procedure_type_id'] + 0;
                    $name = addslashes($grporders[$i]['name']);
                    $codes = addslashes($grporders[$i]['related_code']);
                    echo "opener.set_proc_type($typeid, '$name', '$codes', $t);\n";
                }
                
                $i++;
            } while ($grporders[$i]['procedure_type_id']);
            ?>
        }
        window.close();
        $(document).ready(function () {
            $('.table').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
    <?php
    exit();
}

// End Submission.
//////////////////////////////////////////////////////////////////////

?>
<!DOCTYPE html>
<html>
<head>
    <?php Header::setupHeader(['opener']); ?>
    <title><?php echo xlt('Procedure Picker'); ?></title>

    <script language="JavaScript">
        // Reload the script with the select procedure type ID.
        function selcode(typeid) {
            location.href = 'find_lab_order_popup.php?typeid=' + typeid;
            return false;
        }
    </script>
</head>
<body>
<div class="container">
    <form class="form-inline" method='post' name='theform' action='find_lab_order_popup_modal.php<?php echo "?order=$order&labid=$labid&modality=$modality";
    if (isset($_GET['formid'])) {
        echo '&formid=' . $_GET['formid'];
    }

    if (isset($_GET['formseq'])) {
        echo '&formseq=' . $_GET['formseq'];
    }
    if (isset($_GET['addfav'])) {
        echo '&addfav=' . $_GET['addfav'];
    }
    ?>'>
<!--        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="input-group">
                    <input type="hidden" name='isfav' value='<?php echo attr($_REQUEST['ordLookup']); ?>'>
                    <input class="form-control" id='search_term' name='search_term' value='<?php echo attr($_REQUEST['search_term']); ?>'
                           title='<?php echo xla('Any part of the desired code or its description'); ?>' placeholder="<?php echo xla('Search for') ?>&hellip;"/>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default btn-search" name='bn_search' value="true"><?php echo xla('Search'); ?></button>
                        <?php if (!isset($_REQUEST['addfav'])) { ?>
                            <button type="submit" class="btn btn-default btn-search" name='bn_grpsearch' value="true"><?php echo xla('Favorites'); ?></button>
                        <?php } ?>
                        <button type="button" class="btn btn-default btn-delete" onclick="selcode(0)"><?php echo xla('Erase'); ?></button>
                    </span>
                </div>
            </div>
        </div>-->
        <?php if ($_REQUEST['bn_search'] || $_REQUEST['bn_grpsearch']) { ?>
            <div class="table-responsive">
                <table class="table table-striped table-condensed" id="popupTable">
                    <thead>
                    <th><?php echo xlt('Code'); ?></th>
                    <th><?php echo xlt('Description'); ?></th>
                    </thead>
                    <?php
                    $ord = isset($_REQUEST['bn_search']) ? 'ord' : 'fgp';
                    $search_term = '%' . $_REQUEST['search_term'] . '%';
                    $query = "SELECT * FROM(SELECT DISTINCT sct2_description.id,conceptId, term
                    FROM sct2_relationship JOIN sct2_description ON sct2_description.conceptId= sct2_relationship.sourceId 
                    WHERE sct2_description.active = 1 AND destinationId IN 
                    (SELECT sourceId FROM sct2_relationship WHERE sct2_description.active = 1 AND destinationId = 15220000)) AS res
                                WHERE (conceptId LIKE ? OR term LIKE ?)";                       
                    $res = sqlStatement($query, array($search_term, $search_term));

                    while ($row = sqlFetchArray($res)) {
                         $itertypeid = $row['conceptId'];
                        $itercode = $row['conceptId'];
                        $itertext = trim($row['term']);
                        $anchor = "<a href='' onclick='return selcode(" .
                            "\"" . $itertypeid . "\")'>";
                        echo " <tr>";
                        echo "  <td>$anchor" . text($itercode) . "</a></td>\n";
                        echo "  <td>$anchor" . text($itertext) . "</a></td>\n";
                        echo " </tr>";
                    } ?>
                </table>
            </div>
        <?php }else {?>        
            <div class="table-responsive">
                <table class="table table-striped table-condensed" id="popupTable" cellspacing="0" width="100%">
                    <thead>
                    <th><?php echo xlt('Code'); ?></th>
                    <th><?php echo xlt('Description'); ?></th>
                    </thead>
                    <?php                    
                    $query = "SELECT * FROM (SELECT DISTINCT sct2_description.id,conceptId, term
                    FROM sct2_relationship JOIN sct2_description ON sct2_description.conceptId= sct2_relationship.sourceId 
                    WHERE sct2_description.active = 1 AND destinationId IN 
                    (SELECT sourceId FROM sct2_relationship WHERE sct2_description.active = 1 AND destinationId = 15220000)) as res order by conceptId";
                    $res = sqlStatement($query);

                    while ($row = sqlFetchArray($res)) {
                        $itertypeid = $row['id'];
                        $itercode = $row['conceptId'];
                        $itertext = trim($row['term']);
                        $anchor = "<a href='' onclick='return selcode(" .
                            "\"" . $itertypeid . "\")'>";
                        echo " <tr>";
                        echo "  <td>$anchor" . text($itercode) . "</a></td>\n";
                        echo "  <td>$anchor" . text($itertext) . "</a></td>\n";
                        echo " </tr>";
                    } ?>
                </table>
            </div>
        <?php } ?>

    </form>
</div> 
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script language="JavaScript">
        $(document).ready(function () {
            $('#popupTable').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
</body>
</html>
