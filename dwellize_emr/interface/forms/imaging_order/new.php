<?php
require_once("../../globals.php");
require_once("$srcdir/api.inc");
require_once("$srcdir/forms.inc");
require_once("$srcdir/options.inc.php");
require_once("../../orders/qoe.inc.php");
require_once("../../orders/gen_hl7_imaging_order.inc.php");
require_once("../../../custom/code_types.inc.php");

use OpenEMR\Core\Header;

// Defaults for new orders.
$row = array(
    'provider_id' => $_SESSION['authUserID'],
    'date_ordered' => date('Y-m-d'),
    'date_collected' => date('Y-m-d H:i'),
);

if (!$encounter) { // comes from globals.php
    die("Internal error: we do not seem to be in an encounter!");
}

function cbvalue($cbname)
{
    return $_POST[$cbname] ? '1' : '0';
}

function cbinput($name, $colname)
{
    global $row;
    $ret = "<input type='checkbox' name='$name' value='1'";
    if ($row[$colname]) {
        $ret .= " checked";
    }
    $ret .= " />";
    return $ret;
}

function cbcell($name, $desc, $colname)
{
    return "<td width='25%' nowrap>" . cbinput($name, $colname) . "$desc</td>\n";
}

function QuotedOrNull($fld)
{
    if (empty($fld)) {
        return "NULL";
    }

    return "'$fld'";
}


function getListOptions($list_id, $fieldnames = array('option_id', 'title', 'seq'))
{
    $output = array();
    $query = sqlStatement("SELECT " . implode(',', $fieldnames) . " FROM list_options where list_id = ? AND activity = 1 order by seq", array($list_id));
    while ($ll = sqlFetchArray($query)) {
        foreach ($fieldnames as $val) {
            $output[$ll['option_id']][$val] = $ll[$val];
        }
    }
    return $output;
}

function getDicomModalities()
{
    $output = array();
    $query = sqlStatement("SELECT * FROM dicom_modality");
    while ($ll = sqlFetchArray($query)) {
        array_push($output, $ll);
    }
    return $output;
}


$formid = 0 + (isset($_GET['id']) ? $_GET['id'] : 0);

if ($_POST['bn_save'] || $_POST['bn_xmit']) {
    $ppid = formData('form_lab_id') + 0;
    $modalityValue = formData('modality');
    $accessionNumber = $pid . $encounter. date('YmdHis');
    $sets =
        "date_ordered = " . QuotedOrNull(formData('form_date_ordered')) . ", " .
        "provider_id = " . (formData('form_provider_id') + 0) . ", " .
        "lab_id = " . $ppid . ", " .
        "order_priority = '" . formData('form_order_priority') . "', " .
        "order_status = '" . formData('form_order_status') . "', " .
        "clinical_hx = '" . formData('form_clinical_hx') . "', " .
        "patient_instructions = '" . formData('form_patient_instructions') . "', " .
        "patient_id = '" . $pid . "', " .
        "accession_number = '" . $accessionNumber . "', " .
        "encounter_id = '" . $encounter . "', " .
        "history_order= '" . formData('form_history_order') . "'";

    // If updating an existing form...
    //
    if ($formid) {
        $query = "UPDATE imaging_order SET $sets " .
            "WHERE imaging_order_id = '$formid'";
        sqlStatement($query);
    } // If adding a new form...
    //
    else {
        $query = "INSERT INTO imaging_order SET $sets";
        $formid = sqlInsert($query);
        addForm($encounter, "Imaging Order", $formid, "imaging_order", $pid, $userauthorized);
    }

    //insert data into imaging_study table
    $bodySite = explode("&",formData('form_body_site'));
    $sets =            
        "studyId = '" . formData('form_study_id') . "', " .
        "modality = '" . formData('modality') . "', " .
        "patientId = " . $pid . ", " .
        "conclusion = '" . "" . "', " .
        "bodySite = '" . $bodySite[1] . "', " .
        "encounterId = '" . $encounter . "', " .
        "imaging_order_id = '" . $formid . "', " .
        "accession_number = '" . $accessionNumber . "', " .
        "bodySiteCode = '" . $bodySite[0] .  "'";
    $query = sqlStatement("SELECT id FROM imaging_study WHERE imaging_order_id =". $formid);
    $imageStudyResult = sqlFetchArray($query);
    if($imageStudyResult){
        $query = "UPDATE imaging_study SET $sets " .
            "WHERE imaging_order_id = '$formid'";
        sqlStatement($query);
    }else{
        $query = "INSERT INTO imaging_study SET $sets";
        sqlInsert($query);
    }    
        
    // Remove any existing procedures and their answers for this order and
    // replace them from the form.

    sqlStatement(
        "DELETE FROM imaging_order_code WHERE imaging_order_id = ?",
        array($formid)
    );
    
    for ($i = 0; isset($_POST['form_proc_type'][$i]); ++$i) {
        $ptid = $_POST['form_proc_type'][$i] + 0;
//        if ($ptid <= 0) {
//            continue;
//        }
        
        if(isset($_POST['form_proc_type'][$i])){
            $codeModality = explode('&',$_POST['form_proc_type'][$i]);
        }
        $prefix = "ans$i" . "_";
        //sqlBeginTrans();
        $procedure_order_seq = sqlQuery("SELECT IFNULL(MAX(imaging_order_seq),0) + 1 AS increment FROM imaging_order_code WHERE imaging_order_id = ? ", array($formid));
        $poseq = sqlInsert(
            "INSERT INTO imaging_order_code SET " .
            "imaging_order_id = ?, " .
            "diagnoses = ?, " .
            "reason = ?, " .
            "procedure_order_title = ?, " .
            "modality = ?, " .
            "procedure_code = ?, " .
            "procedure_name = ?," .
            "imaging_order_seq = ? ",
            array($formid, $_POST['form_proc_type_diag'][$i],$_POST['form_proc_diag'][$i], $_POST['form_proc_order_title'][$i],'CT', $_POST['form_proc_type_desc'][$i], $_POST['form_proc_type'][$i], $procedure_order_seq['increment'])
        );
        $query = "UPDATE imaging_study SET modality ='$codeModality[1]' WHERE imaging_order_id = '$formid'";
        sqlStatement($query);
        //sqlCommitTrans();

        // can't get insert_id on a table w/o an auto increment column so below. sjp
        $poseq = $procedure_order_seq['increment'];

    }

    $alertmsg = '';
    if ($_POST['bn_xmit']) {
        $hl7 = '';
        $alertmsg = gen_hl7_order($formid, $hl7);
        if (empty($alertmsg)) {
            $alertmsg = send_hl7_order($ppid, $hl7);
        }

        if (empty($alertmsg)) {
            sqlStatement("UPDATE imaging_order SET date_transmitted = NOW() WHERE " .
                "imaging_order_id = ?", array($formid));
        }
    }

    formHeader("Redirecting....");
    if ($alertmsg) {
        echo "\n<script language='Javascript'>alert('";
        echo addslashes(xl('Transmit failed') . ': ' . $alertmsg);
        echo "')</script>\n";
    }

    formJump();
    formFooter();
    exit;
}

if ($formid) {
    $row = sqlQuery(
        "SELECT * FROM imaging_order WHERE " .
        "imaging_order_id = ?",
        array($formid)
    );
    
    $imageStudyQuery = sqlStatement("SELECT * FROM imaging_study WHERE imaging_order_id =". $formid);                        
    $imageStudyResult = sqlFetchArray($imageStudyQuery);
    $bodySiteCode = $imageStudyResult['bodySiteCode']."&".$imageStudyResult['bodySite'];
}

$enrow = sqlQuery(
    "SELECT p.fname, p.mname, p.lname, fe.date FROM " .
    "form_encounter AS fe, forms AS f, patient_data AS p WHERE " .
    "p.pid = ? AND f.pid = p.pid AND f.encounter = ? AND " .
    "f.formdir = 'newpatient' AND f.deleted = 0 AND " .
    "fe.id = f.form_id LIMIT 1",
    array($pid, $encounter)
);

?>
<!DOCTYPE html>
<html>

<head>
    <?php Header::setupHeader(['datetime-picker']); ?>

    <script type="text/javascript">
        var gbl_formseq;

        function initCalendars() {
            var datepicker = {
                <?php $datetimepicker_timepicker = false; ?>
                <?php $datetimepicker_showseconds = false; ?>
                <?php $datetimepicker_formatInput = false; ?>
                <?php require($GLOBALS['srcdir'] . '/js/xl/jquery-datetimepicker-2-5-4.js.php'); ?>
            };
            var datetimepicker = {
                <?php $datetimepicker_timepicker = true; ?>
                <?php $datetimepicker_showseconds = false; ?>
                <?php $datetimepicker_formatInput = false; ?>
                <?php require($GLOBALS['srcdir'] . '/js/xl/jquery-datetimepicker-2-5-4.js.php'); ?>
            };
            $('.datepicker').datetimepicker(datepicker);
            $('.datetimepicker').datetimepicker(datetimepicker);
        }

        // This invokes the find-procedure-type popup.
        // formseq = 0-relative index in the form.
        function sel_proc_type(formseq) {
            let f = document.forms[0];
            gbl_formseq = formseq;
            let ptvarname = 'form_proc_type[' + formseq + ']';

            let title = '<?php echo xla("Find Procedure Order"); ?>';
            // This replaces the previous search for an easier/faster order picker tool.
            dlgopen('../../orders/find_imaging_order_popup.php' +
                '?labid=' + f.form_lab_id.value +
                '&order=' + f[ptvarname].value +
                '&modality=' + f.modality.value +
                '&formid=<?php echo $formid; ?>' +
                '&formseq=' + formseq,
                '_blank', 850, 500, '', title);
        }
        
        function sel_image_type(formseq) {
//            let f = document.forms[0];
//            gbl_formseq = formseq;
//            let ptvarname = 'form_proc_type[' + formseq + ']';
//
//            let title = '<?php echo xla("Find Image Order"); ?>';
            // This replaces the previous search for an easier/faster order picker tool.
            //dlgopen('../../orders/find_imaging_order_popup_modal.php','_blank', 1250, 500, '', title);
             rcvarname = formseq;
            // codetype is just to make things easier and avoid mistakes.
            // Might be nice to have a lab parameter for acceptable code types.
            // Also note the controlling script here runs from interface/patient_file/encounter/.
            let title = '<?php echo xla("Select Image Order"); ?>';
            dlgopen('find_image_dynamic.php?codetype=<?php echo attr(collect_codetypes("diagnosis", "csv")); ?>', '_blank', 985, 750, '', title);
        }

        // This is for callback by the find-procedure-type popup.
        // Sets both the selected type ID and its descriptive name.
        // Also set diagnosis if supplied in configuration and custom test groups.
        function set_proc_type(typeid, typename, diagcodes = '', newCnt = 0) {
            let f = document.forms[0];
            let ptvarname = 'form_proc_type[' + gbl_formseq + ']';
            let ptdescname = 'form_proc_type_desc[' + gbl_formseq + ']';
            let ptcodes = 'form_proc_type_diag[' + gbl_formseq + ']';
            f[ptvarname].value = typeid;
            f[ptdescname].value = typename;
            if (diagcodes)
                //f[ptcodes].value = diagcodes;
            if (newCnt > 1) {
                gbl_formseq = addProcLine(true);
            }
        }

        // This is also for callback by the find-procedure-type popup.
        // Sets the contents of the table containing the form fields for questions.
        function set_proc_html(s, js) {
            document.getElementById('qoetable[' + gbl_formseq + ']').innerHTML = s;
            initCalendars();
        }

        // New lab selected so clear all procedures and questions from the form.
        function lab_id_changed() {
            let f = document.forms[0];
            for (let i = 0; true; ++i) {
                let ix = '[' + i + ']';
                if (!f['form_proc_type' + ix]) break;
                f['form_proc_type' + ix].value = '-1';
                f['form_proc_type_desc' + ix].value = '';
                document.getElementById('qoetable' + ix).innerHTML = '';
            }
        }

        function modality_changed() {
            let f = document.forms[0];
            for (let i = 0; true; ++i) {
                let ix = '[' + i + ']';
                if (!f['form_proc_type' + ix]) break;
                f['form_proc_type' + ix].value = '-1';
                f['form_proc_type_desc' + ix].value = '';
                document.getElementById('qoetable' + ix).innerHTML = '';
            }
        }

        function addProcedure() {
            $(".procedure-order-container").append($(".procedure-order").clone());
            let newOrder = $(".procedure-order-container .procedure-order:last");
            $(newOrder + " label:first").append("1");
        }

        function addProcLine(flag = false) {
            let f = document.forms[0];
            let e = document.getElementById("modality");
            console.log(e);
            let prc_name = e.options[e.selectedIndex].value;
            // Compute i = next procedure item index.
            let i = 0;
            for (; f['form_proc_type[' + i + ']']; ++i) ;
            // build new item html.. a hidden html block to clone may be better here.
            let cell = "<table class='table table-condensed proc-table'><tr><td onclick='deleteRow(event)' class='itemDelete'><i class='fa fa-remove'></i></td>" +
                "<td class='procedure-div'><input type='hidden' name='form_proc_order_title[" + i + "]' value='" + prc_name + "'>" +
                "<input type='text' class='form-control' name='form_proc_type_desc[" + i + "]' onclick='sel_proc_type(" + i + ")' " +
                "onfocus='this.blur()' title='<?php echo xla('Click to select the desired procedure'); ?>' style='cursor:pointer;cursor:hand' readonly /> " +
                "<input type='hidden' name='form_proc_type[" + i + "]' value='-1' /></td>" +
                "<td class='diagnosis-div'><input type='text' class='form-control' name='form_proc_type_diag[" + i + "]' onclick='sel_related(this.name)'" +
                "title='<?php echo xla('Click to add a diagnosis'); ?>' onfocus='this.blur()' style='cursor:pointer;cursor:hand' readonly /></td>" +
                "<td><div id='qoetable[" + i + "]'></div></td></tr></table>";

            $(".procedure-order-container").append(cell); // add the new item to procedures list

            if (!flag) {// flag true indicates add procedure item from custom group callback with current index.
                sel_proc_type(i);
                return false;
            } else {
                return i;
            }
        }

        // The name of the form field for find-code popup results.
        var rcvarname;

        // This is for callback by the find-code popup.
        // Appends to or erases the current list of related codes.
        function set_related(codetype, code, selector, codedesc) {
            var f = document.forms[0];
            var s = f['form_proc_type_desc['+rcvarname+']'].value;
            if (code) {
                if (s.length > 0) s += ';';
                s = codetype + ':' + code;
            } else {
                s = '';
            }
            f['form_proc_type_desc['+rcvarname+']'].value = s;
            f['form_proc_type['+rcvarname+']'].value = codedesc;
        }
        
        function set_related_reason(codetype, code, selector, codedesc) {
            var f = document.forms[0];
            var s = f[rcvarname].value;
            if (code) {
                if (s.length > 0) s += ';';
                s = codetype + ':' + code;
            } else {
                s = '';
            }
            f[rcvarname].value = s;
            f['form_proc_diag[0]'].value = codedesc;
            f['form_proc_type_diag[0]'].value = s;
        }

        // This invokes the find-code popup.
        function sel_related(varname) {
            rcvarname = varname;
            // codetype is just to make things easier and avoid mistakes.
            // Might be nice to have a lab parameter for acceptable code types.
            // Also note the controlling script here runs from interface/patient_file/encounter/.
            let title = '<?php echo xla("Select Reason"); ?>';
            dlgopen('find_reason_dynamic.php?codetype=<?php echo attr(collect_codetypes("diagnosis", "csv")); ?>', '_blank', 985, 750, '', title);
        }

        var transmitting = false;

        // Issue a Cancel/OK warning if a previously transmitted order is being transmitted again.
        function validate(f) {
            <?php if (!empty($row['date_transmitted'])) { ?>
            if (transmitting) {
                if (!confirm('<?php echo xls('This order was already transmitted on') . ' ' .
                    addslashes($row['date_transmitted']) . '. ' .
                    xls('Are you sure you want to transmit it again?'); ?>')) {
                    return false;
                }
            }
            <?php } ?>
            top.restoreSession();
            return true;
        }

        $(document).ready(function () {
            // calendars need to be available to init dynamically for procedure item adds.
            initCalendars();
            initDeletes();
        });
    </script>
    <style>
        @media only screen and (max-width: 768px) {
            [class*="col-"] {
                width: 100%;
                text-align: left !Important;
            }
        }

        .qoe-table {
            margin-bottom: 0px;
        }

        .proc-table {
            margin-bottom: 5px;
        }

        .proc-table .itemDelete {
            width: 25px;
            color: #FF0000;

            cursor:pointer;
            cursor:hand;
        }

        /* leave below styles. may use future */
        /*.proc-table .procedure-div {
            min-width: 20%;
        }

        .proc-table .diagnosis-div{
            min-width: 20%;
        }*/
    </style>
    <?php
    $name = $enrow['fname'] . ' ';
    $name .= (!empty($enrow['mname'])) ? $enrow['mname'] . ' ' . $enrow['lname'] : $enrow['lname'];
    $date = xl('on') . ' ' . oeFormatShortDate(substr($enrow['date'], 0, 10));
    $title = array(xl('Image Order for'), $name, $date);
    //echo join(" ", $title);
    ?>
</head>
<body class="body_top">
<div class="container">
    <div class="row">
        <div>
            <div class="page-header">
                <h2><?php echo join(" ", $title); ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
    <form class="form-horizontal" method='post' action="<?php echo $rootdir ?>/forms/imaging_order/new.php?id=<?php echo $formid ?>"
          onsubmit='return validate(this)'>
        <input type="hidden" name="csrf_token_form" value="<?php echo attr(collectCsrfToken()); ?>" />
        <fieldset>
            <legend><?php echo xlt('Select Options for Current Image Order Id ') . (text($formid) ? text($formid) : 'New Order')?></legend>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="provider_id" class="control-label col-sm-3 oe-text-to-right"><?php xl('Ordering Provider', 'e'); ?></label>
                    <div class="col-sm-2">
                        <?php generate_form_field(array('data_type' => 10, 'field_id' => 'provider_id'), $row['provider_id']); ?>
                    </div>
                    <label for="lab_id" class="control-label col-sm-3 oe-text-to-right"><?php xl('Sending To', 'e'); ?></label>
                    <div class="col-sm-2">
                        <select name='form_lab_id' onchange='lab_id_changed()' class='form-control'>
                            <?php
                            $ppres = sqlStatement("SELECT ppid, name FROM procedure_providers " .
                                "ORDER BY name, ppid");
                            while ($pprow = sqlFetchArray($ppres)) {
                                echo "<option value='" . attr($pprow['ppid']) . "'";
                                if ($pprow['ppid'] == $row['lab_id']) {
                                    echo " selected";
                                }
                                echo ">" . text($pprow['name']) . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="form_data_ordered" class="control-label col-sm-3 oe-text-to-right"><?php xl('Order Date', 'e'); ?></label>
                    <div class="col-sm-2">
                        <input type='text'
                               class='datepicker form-control'
                               name='form_date_ordered'
                               id='form_date_ordered'
                               value="<?php echo $row['date_ordered']; ?>"
                               title="<?php xl('Date of this order', 'e'); ?>"/>
                    </div>
                    <label for="form_data_ordered" class="control-label col-sm-3 oe-text-to-right"><?php xl('Internal Time Collected', 'e'); ?></label>
                    <div class="col-sm-2">
                        <input class='datetimepicker form-control'
                               type='text'
                               name='form_date_collected'
                               id='form_date_collected'
                               value="<?php echo substr($row['date_collected'], 0, 16); ?>"
                               title="<?php xl('Date and time that the sample was collected', 'e'); ?>"/>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="form_data_ordered" class="control-label col-sm-3 oe-text-to-right"><?php xl('Priority', 'e'); ?></label>
                    <div class="col-sm-2">
                        <?php
                        generate_form_field(array('data_type' => 1, 'field_id' => 'order_priority',
                            'list_id' => 'ord_priority'), $row['order_priority']);
                        ?>
                    </div>

                    <label for="form_data_ordered" class="control-label col-sm-3 oe-text-to-right"><?php xl('Status', 'e'); ?></label>
                    <div class="col-sm-2">
                        <?php
                        generate_form_field(array('data_type' => 1, 'field_id' => 'order_status',
                            'list_id' => 'ord_status'), $row['order_status']);
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="form_data_ordered" class="control-label col-sm-3 oe-text-to-right"><?php xl('History Order', 'e'); ?></label>
                    <div class="col-sm-2">
                        <?php
                        $historyOrderOpts = array(
                            'data_type' => 1,
                            'field_id' => 'history_order',
                            'list_id' => 'boolean'
                        );
                        generate_form_field($historyOrderOpts, $row['history_order']); ?>
                    </div>
                    <label for="form_data_ordered" class="control-label col-sm-3 oe-text-to-right"><?php xl('Study Id(for demo only)', 'e'); ?></label>
                    <div class="col-sm-2">
                        <input type='text' class='form-control' name='form_study_id' value="<?php echo $imageStudyResult['studyId']; ?>" id='form_study_id' title="<?php xl('Study id for this order', 'e'); ?>"/>
                   </div>                    
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="form_data_ordered" class="control-label col-sm-3 oe-text-to-right"><?php xl('Body Site', 'e'); ?></label>
                    <div class="col-sm-2">
                        <select name='form_body_site' class='form-control'>
                            <?php
                            $result = sqlStatement("SELECT title,codes FROM list_options WHERE list_id = 'Body_Sites' AND activity = 1 order by title;");
                            while ($rowBodySite = sqlFetchArray($result)) {
                                $code = attr($rowBodySite['codes'])."&". attr($rowBodySite['title']);                                
                                echo "<option value='" . attr($code) . "'";
                                if ($code == $bodySiteCode) {
                                    echo " selected";
                                }
                                echo ">" . text($rowBodySite['title']) . "</option>";
                            }
                            ?>
                        </select>
                    </div>                                      
                    <div class="clearfix"></div>
                </div>
                <div class="form-group">
                    <label for="form_clinical_hx" class="control-label col-sm-3"><?php xl('Clinical History', 'e'); ?></label>
                    <div class="col-sm-7">
                        <textarea name="form_clinical_hx" id="form_clinical_hx" class="form-control" rows="2"><?php echo $row['clinical_hx']; ?></textarea>
                    </div>
                </div>
                <?php // Hide this for now with a hidden class as it does not yet do anything ?>
                <div class="form-group hidden">
                    <label for="form_data_ordered" class="control-label col-sm-3 oe-text-to-right"><?php xl('Patient Instructions', 'e'); ?></label>
                    <div class="col-sm-7">
                            <textarea rows='3' cols='35' name='form_patient_instructions' wrap='virtual' class='form-control inputtext'>
                                <?php echo $row['patient_instructions'] ?>
                            </textarea>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend><?php xl('Imaging Order Details', 'e'); ?></legend>
                <?php
                // This section merits some explanation. :)
                //
                // If any procedures have already been saved for this form, then a top-level table row is
                // created for each of them, and includes the relevant questions and any existing answers.
                // Otherwise a single empty table row is created for entering the first or only procedure.
                //
                // If a new procedure is selected or changed, the questions for it are (re)generated from
                // the dialog window from which the procedure is selected, via JavaScript.  The sel_proc_type
                // function and the types.php script that it invokes collaborate to support this feature.
                //
                // The generate_qoe_html function in qoe.inc.php contains logic to generate the HTML for
                // the questions, and can be invoked either from this script or from types.php.
                //
                // The $i counter that you see below is to resolve the need for unique names for form fields
                // that may occur for each of the multiple procedure requests within the same order.
                // procedure_order_seq serves a similar need for uniqueness at the database level.

                $oparr = array();
                if ($formid) {
                    $opres = sqlStatement(
                        "SELECT " .
                        "pc.imaging_order_seq, pc.modality, pc.procedure_code, pc.procedure_name, " .
                        "pc.reason, pc.diagnoses,pc.procedure_order_title, " .
                        // In case of duplicate procedure codes this gets just one.
                        "(SELECT pt.procedure_type_id FROM procedure_type AS pt WHERE " .
                        "(pt.procedure_type LIKE 'ord%' OR pt.procedure_type LIKE 'for%') AND pt.lab_id = ? AND " .
                        "pt.procedure_code = pc.procedure_code ORDER BY " .
                        "pt.activity DESC, pt.procedure_type_id LIMIT 1) AS procedure_type_id " .
                        "FROM imaging_order_code AS pc " .
                        "WHERE pc.imaging_order_id = ? " .
                        "ORDER BY pc.imaging_order_seq",
                        array($row['lab_id'], $formid)
                    );
                    while ($oprow = sqlFetchArray($opres)) {
                        $oparr[] = $oprow;
                    }
                }
                if (empty($oparr)) {
                    $oparr[] = array('procedure_name' => '');
                }
                ?>
                <?php
                $i = 0;
                foreach ($oparr as $oprow) {
                    $ptid = -1; // -1 means no procedure is selected yet
                    if (!empty($oprow['procedure_type_id'])) {
                        $ptid = $oprow['procedure_type_id'];
                    }
                    ?>
            <div class="col-md-12 procedure-order-container table-responsive">
<!--                <div class="form-group">
                    <?php $procedure_order_type = getDicomModalities(); ?>
                    <label for="modality" class="control-label col-sm-2 col-sm-offset-3"><?php xl('Modality', 'e'); ?></label>
                    <div class="col-sm-3">
                        <select name="modality" id="modality" onchange="modality_changed()" class='form-control'>
                            <?php foreach ($procedure_order_type as $ordered_types) {
                                echo "<option value='" . attr($ordered_types['code']) . "'";
                                if ($ordered_types['code'] == $oprow['modality']) {
                                    echo " selected";
                                }
                                echo ">" . text($ordered_types['name']) . "</option>";

                            } ?>
                        </select>
                    </div>
                </div>-->
                    <table class="table table-condensed proc-table" id="procedures_item_<?php echo (string) attr($i) ?>">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th><?php echo xlt('Imaging Test'); ?></th>
                            <th><?php echo xlt('Reason'); ?></th>
                            <th><?php echo xlt("Order Questions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="itemDelete"><i class="fa fa-remove fa-lg"></i></td>
                            <td class="procedure-div">
                                <?php if (empty($formid) || empty($oprow['procedure_order_title'])) : ?>
                                    <input type="hidden" name="form_proc_order_title[<?php echo $i; ?>]" value="Procedure">
                                <?php else : ?>
                                    <input type='hidden' name='form_proc_order_title[<?php echo $i; ?>]' value='<?php echo attr($oprow['procedure_order_title']) ?>'>
                                <?php endif; ?>
                                <input type='text' name='form_proc_type[<?php echo $i; ?>]'
                                       value='<?php echo attr($oprow['procedure_name']) ?>'
                                       onclick="sel_image_type(<?php echo $i; ?>)"
                                       onfocus='this.blur()'
                                       title='<?php xla('Click to select the desired procedure', 'e'); ?>'
                                       placeholder='<?php xla('Click to select the desired procedure', 'e'); ?>'
                                       style='cursor:pointer;cursor:hand' class='form-control' readonly/>
                                <input type='hidden' name='form_proc_type_desc[<?php echo $i; ?>]' value='<?php echo attr($oprow['procedure_code']) ?>'/>
                            </td>
                            <td class="diagnosis-div">
                                <input class='form-control' type='text' name='form_proc_diag[<?php echo $i; ?>]'
                                       value='<?php echo attr($oprow['reason']) ?>' onclick='sel_related(this.name)'
                                       title='<?php echo xla('Click to add a diagnosis'); ?>'
                                       onfocus='this.blur()'
                                       style='cursor:pointer;cursor:hand' readonly/>
                                <input type='hidden' name='form_proc_type_diag[<?php echo $i; ?>]' value='<?php echo attr($oprow['diagnoses']) ?>'/>
                            </td>
                            <td>
                                <!-- MSIE innerHTML property for a TABLE element is read-only, so using a DIV here. -->
                                <div class="table-responsive" id='qoetable[<?php echo $i; ?>]'>
                                    <?php
                                    $qoe_init_javascript = '';
                                    echo generate_qoe_html($ptid, $formid, $oprow['procedure_order_seq'], $i);
                                    if ($qoe_init_javascript) {
                                        echo "<script language='JavaScript'>$qoe_init_javascript</script>";
                                    }
                                    ?>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <?php
                    ++$i;
                }
                ?>
        </fieldset>
        <?php //can change position of buttons by creating a class 'position-override' and adding rule text-alig:center or right as the case may be in individual stylesheets ?>
        <div class="form-group clearfix">
            <div class="col-sm-12 text-left position-override">
                <div class="btn-group btn-group-pinch" role="group">
<!--                    <button type="button" class="btn btn-default btn-add" onclick="addProcLine()">--><?php //echo xla('Add Procedure'); ?><!--</button>-->
                    <button type="submit" class="btn btn-default btn-save" name='bn_save' value="save" onclick='transmitting = false;'><?php echo xla('Save'); ?></button>
                    <button type="submit" class="btn btn-default btn-transmit" name='bn_xmit' value="transmit" onclick='transmitting = true;'><?php echo xla('Save and Transmit'); ?></button>
                    <button type="button" class="btn btn-link btn-cancel btn-separate-left" onclick="top.restoreSession();location='<?php echo $GLOBALS['form_exit_url']; ?>'"><?php echo xla('Cancel'); ?></button>
                </div>
            </div>
        </div>
    </form>
</div>
</div>
</div><!--end of .container -->
</body>
</html>