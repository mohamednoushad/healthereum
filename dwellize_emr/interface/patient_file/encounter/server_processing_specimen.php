<?php

require_once('../../globals.php');
require_once($GLOBALS['srcdir'].'/patient.inc');
require_once($GLOBALS['srcdir'].'/csv_like_join.php');
require_once($GLOBALS['fileroot'].'/custom/code_types.inc.php');

$columns = array( 
	0 =>'loinc_num', 
	1 => 'shortname'
);

if(!empty($_POST['search']['value'])) {
	// getting total number records without any search
        $sql = "SELECT COUNT(DISTINCT conceptId, term) as count
            FROM sct2_relationship JOIN sct2_description ON sct2_description.conceptId= sct2_relationship.sourceId WHERE sct2_description.active = 1 AND destinationId IN
            (SELECT DISTINCT sourceId FROM sct2_relationship WHERE  destinationId IN 
            (SELECT sourceId FROM sct2_relationship WHERE sct2_description.active = 1 AND destinationId = 123038009)) AND (conceptId like '%".$_POST['search']['value']."%' OR term like '%".$_POST['search']['value']."%');";        
        $res = sqlStatement($sql);
        while ($row = sqlFetchArray($res)) {
            $totalData = $row['count'];
            $totalFiltered = $row['count'];
        }
        $sql = "SELECT DISTINCT conceptId as code, term as code_text
            FROM sct2_relationship JOIN sct2_description ON sct2_description.conceptId= sct2_relationship.sourceId WHERE sct2_description.active = 1 AND destinationId IN
            (SELECT DISTINCT sourceId FROM sct2_relationship WHERE  destinationId IN 
            (SELECT sourceId FROM sct2_relationship WHERE sct2_description.active = 1 AND destinationId = 123038009)) AND (conceptId like '%".$_POST['search']['value']."%' OR term like '%".$_POST['search']['value']."%') LIMIT ".$_POST['start']." ,".$_POST['length'];	
	
} else {
        // getting total number records without any search
        $sql = "SELECT COUNT(DISTINCT conceptId, term) as count
                FROM sct2_relationship JOIN sct2_description ON sct2_description.conceptId= sct2_relationship.sourceId WHERE sct2_description.active = 1 AND destinationId IN
                (SELECT DISTINCT sourceId FROM sct2_relationship WHERE  destinationId IN 
                (SELECT sourceId FROM sct2_relationship WHERE sct2_description.active = 1 AND destinationId = 123038009));";        
        $res = sqlStatement($sql);
        while ($row = sqlFetchArray($res)) {
            $totalData = $row['count'];
            $totalFiltered = $row['count'];
        }
        $sql = "SELECT DISTINCT conceptId as code, term as code_text
                FROM sct2_relationship JOIN sct2_description ON sct2_description.conceptId= sct2_relationship.sourceId WHERE sct2_description.active = 1 AND destinationId IN
                (SELECT DISTINCT sourceId FROM sct2_relationship WHERE  destinationId IN 
                (SELECT sourceId FROM sct2_relationship WHERE sct2_description.active = 1 AND destinationId = 123038009)) LIMIT ".$_POST['start']." ,".$_POST['length'];		
}
$data = array();
$res = sqlStatement($sql);
while( $row=sqlFetchArray($res)) {  // preparing an array
	$nestedData=array(); 
	$nestedData[] = "<a href='' onclick='return selcode(" . attr_js('LOINC') . ", " . attr_js($row["code"]) . ", \"\", " . attr_js($row["code_text"]) . ")'>".$row["code"]."</a>";
	$nestedData[] = "<a href='' onclick='return selcode(" . attr_js('LOINC') . ", " . attr_js($row["code"]) . ", \"\", " . attr_js($row["code_text"]) . ")'>".$row["code_text"]."</a>";
	$data[] = $nestedData;
}
$json_data = array(
			"draw"            => intval( $_POST['draw'] ),  
			"recordsTotal"    => intval( $totalData ), 
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data   // total data array
			);
echo json_encode($json_data);  // send data as json format
?>