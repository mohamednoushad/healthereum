<?php 
require_once("../globals.php");
?>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo $GLOBALS['assets_static_relative'];?>/bootstrap/dist/css/bootstrap.min.css?v=42" type="text/css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['webroot'];?>/public/themes/style_light.css?v=42?v=42" type="text/css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['assets_static_relative'];?>/font-awesome/css/font-awesome.min.css?v=42" type="text/css">
        <link rel="stylesheet" href="<?php echo $GLOBALS['assets_static_relative'];?>/jquery-datetimepicker/build/jquery.datetimepicker.min.css?v=42" type="text/css">

        <script type="text/javascript" src="<?php echo $GLOBALS['assets_static_relative'];?>/jquery/dist/jquery.min.js?v=42"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['assets_static_relative'];?>/bootstrap/dist/js/bootstrap.min.js?v=42"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['assets_static_relative'];?>/jquery-ui/jquery-ui.min.js?v=42"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['assets_static_relative'];?>/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js?v=42"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'];?>/library/textformat.js?v=42"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'];?>/library/dialog.js?v=42"></script>
        <link rel="stylesheet" href="<?php echo $GLOBALS['assets_static_relative'];?>/dropzone/dist/dropzone.css">

        <style type="text/css">
            .warn_diagnostic {
                margin: 10 auto 10 auto;
                color: rgb(255, 0, 0);
                font-size: 1.5em;
            }
            .ui-autocomplete {
                position: absolute;
                top: 0;
                left: 0;
                min-width:200px;
                cursor: default;
            }
            .ui-menu-item{
                 min-width:200px;
            }
            .fixed-height{
                min-width:200px;
                padding: 1px;
                max-height: 35%;
                overflow: auto;
            }
        </style>

        <script type="text/javascript" src="<?php echo $GLOBALS['webroot'];?>/library/js/DocumentTreeMenu.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['assets_static_relative'];?>/dropzone/dist/dropzone.js"></script>

        <title>Imaging-Pending Review</title>
    </head>
    <?php
        if(isset($_GET['delete']) && !empty($_GET['delete'])){            
            $id = $_GET['delete'];
            sqlStatement("DELETE FROM imaging_study_instances WHERE instance_number = $id");
        }
        if ($_POST['form_submit']){
            if(isset($_POST['form_proc_type_diag']) && !empty($_POST['form_proc_type_diag'] && !empty($_POST['study_id']))){
                $studyId = $_POST['study_id'];
                sqlStatement("DELETE FROM imaging_study_conclusion_codes WHERE study_id = $studyId");
                $conclusionCodeArray = explode(";",$_POST['form_proc_type_diag']);
                $conclusionCodeDescArray = explode(';', $_POST['conclusion_code_desc']);            
                for($i=0;$i<count($conclusionCodeArray);$i++){
                    $sets = "conclusion_code = '$conclusionCodeArray[$i]', " .
                            "conclusion_code_term = '$conclusionCodeDescArray[$i]', " .
                            "study_id = '" . $studyId . "'";            
                    sqlInsert("INSERT INTO imaging_study_conclusion_codes SET $sets");               
                }       
            }
            if(isset($_POST['conclusion']) && !empty($_POST['conclusion']) && !empty($_POST['study_id'])){
                    $studyId = $_POST['study_id'];
                    $sets = "conclusion = '" . $_POST['conclusion'] . "',review_status=1";
                    sqlStatement("UPDATE imaging_study SET $sets WHERE id = '$studyId'");
            }
        }
        
        function getReview($pid){
            $output = array();
            $query = "SELECT imaging_study.review_status,imaging_study.id, study_instance_uid, study_date, study_time, study_description, series_instance_id, series_modaliity, series_description, series_date, series_time, instance_uid, instance_number, series_body_site,
                        imaging_study.patientId, imaging_study.accession_number, imaging_study.bodySiteCode, imaging_study.bodySite, imaging_study.encounterId, imaging_study.conclusion,
                        imaging_study_conclusion_codes.codeId AS conclusion_code_id, imaging_study_conclusion_codes.conclusion_code, imaging_study_conclusion_codes.conclusion_code_term,
                        imaging_order_code.procedure_code, imaging_order_code.procedure_name, imaging_order_code.diagnoses AS reasonCode, imaging_order_code.reason,
                        referrer.id AS referrerId, referrer.fname AS referrerFirstName,referrer.lname AS referrerLastName,
                        interpreter.id AS interpreterId, interpreter.fname AS interpreterFirstName,interpreter.lname AS interpreterLastName
                        FROM openemr.imaging_study_instances JOIN imaging_study ON imaging_study_instances.study_instance_uid = imaging_study.studyId
                        JOIN imaging_order ON imaging_study.imaging_order_id = imaging_order.imaging_order_id
                        LEFT JOIN imaging_order_code ON imaging_order_code.imaging_order_id = imaging_order.imaging_order_id
                        LEFT JOIN imaging_study_conclusion_codes ON imaging_study_conclusion_codes.study_id = imaging_study.id
                        LEFT JOIN users AS referrer ON imaging_order.provider_id = referrer.id
                        LEFT JOIN users AS interpreter ON imaging_order.provider_id = interpreter.id
                        WHERE imaging_study.review_status = 1 AND imaging_study.patientId = $pid;";
            
            $result = sqlStatement($query);
            while ($row = sqlFetchArray($result)) {
                array_push($output, $row);
            }
            return $output;            
        }
        
        function processReviews($reviews){
            $studyIds = array();
            $seriesIds =array();
            $instanceIds = array();
            foreach($reviews as $review){
                if(!in_array($review['study_instance_uid'],$studyIds)){
                    $studyIds[] = $review['study_instance_uid'];
                    $studyResults[$review['study_instance_uid']] = array($review['procedure_code'],$review['study_date'],$review['id'],$review['study_time'],$review['accession_number'],$review['study_description'],$review['reasonCode'],$review['reason'],$review['conclusion'],$review['conclusion_code'],$review['conclusion_code_term']);                   
                }
                if(!in_array($review['series_instance_id'],$seriesIds[$review['study_instance_uid']])){
                    $seriesIds[$review['study_instance_uid']][] = $review['series_instance_id'];
                    $seriesResults[$review['series_instance_id']] = array($review['series_modaliity'],$review['series_description'],$review['series_date'],$review['series_time'],$review['bodySite'],$review['bodySiteCode']);
                }
                if(!in_array(review['instance_uid'],$instanceIds[$review['series_instance_id']])){
                    $instanceIds[$review['series_instance_id']][] = $review['instance_uid'];
                    $instanceResults[$review['instance_uid']] = array($review['instance_number'],$review['conclusion'],$review['conclusion_code_term'],$review['study_instance_uid'],$review['series_instance_id']);
                }                
            }
                        
            $results['studyId'] = $studyIds;
            $results['seriesId'] = $seriesIds;
            $results['instanceId'] = $instanceIds;
            $results['studyResults'] = $studyResults;
            $results['seriesResults'] = $seriesResults;
            $results['instanceResults'] = $instanceResults;
            return $results;
        }
        
        $reviews = getReview($pid); 
        $results = processReviews($reviews);
    ?>
    <body class="body_top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header">
                        <h2>Imaging Report</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div id="documents_list">
                        <fieldset>
                            <legend>Imaging Report List</legend>
                            <div style="padding: 0 10px">
                                <?php 
                                    $node0 = 0;
                                    foreach($results['studyId'] as $studyId){
                                        $node0++;
                                ?>
                                    <div id="objTreeMenu_1_node_1_<?php echo $node0; ?>" style="display: inline" class="treeMenuDefault">
                                        <nobr>
                                            <img src="<?php echo $GLOBALS['webroot'];?>/images/plus.gif" width="20" height="20" align="top" border="0" name="img_objTreeMenu_1_node_1_<?php echo $node0; ?>" onmousedown="objTreeMenu_1.toggleBranch('objTreeMenu_1_node_1_<?php echo $node0; ?>', true)" style="cursor: pointer; cursor: hand">
                                            <img src="<?php echo $GLOBALS['webroot'];?>/images/folder.gif" width="20" height="20" align="top" id="icon_objTreeMenu_1_node_1_<?php echo $node0; ?>">                                            
                                            <span id="<?php echo $studyId ; ?>" onclick="loadStudyResult(this.id);">
                                                <?php 
                                                    foreach($results['studyResults'] as $key=>$value){
                                                        if($key == $studyId){
                                                            echo '<a style="cursor:pointer;"><span id="'.$key.'" onclick="loadStudyResult(this.id);">'.$value[0]."::".$value[1].'</span>,</a>'; 
                                                        }
                                                    }
                                                ?>
                                            </span></a>                                            
                                        </nobr><br>
                                    </div>
                                    <?php
                                        $node1 = 0;
                                        foreach($results['seriesId'][$studyId] as $seriesId){
                                            $node1++;
                                    ?>
                                            <div id="objTreeMenu_1_node_1_<?php echo $node0; ?>_<?php echo $node1; ?>" style="display: inline;" class="treeMenuDefault">
                                                <nobr>
                                                    <img src="<?php echo $GLOBALS['webroot'];?>/images/line.gif" width="20" height="20" align="top">
                                                    <img src="<?php echo $GLOBALS['webroot'];?>/images/plusbottom.gif" width="20" height="20" align="top" border="0" name="img_objTreeMenu_1_node_1_<?php echo $node0; ?>_<?php echo $node1; ?>" onmousedown="objTreeMenu_1.toggleBranch('objTreeMenu_1_node_1_<?php echo $node0; ?>_<?php echo $node1; ?>', true)" style="cursor: pointer; cursor: hand">
                                                    <img src="<?php echo $GLOBALS['webroot'];?>/images/folder.gif" width="20" height="20" align="top" id="icon_objTreeMenu_1_node_1_<?php echo $node0; ?>_<?php echo $node1; ?>">                                                    
                                                    <?php 
                                                        foreach($results['seriesResults'] as $key=>$value){
                                                            if($key == $seriesId){
                                                                echo '<a style="cursor:pointer;"><span id="'.$key.'"  onclick="loadSeriesResult(this.id);">'.$value[0].'<span></a>'; 
                                                            }
                                                        }
                                                    ?>
                                                </nobr><br>
                                            </div>
                                            <?php
                                                $node2 = 0;
                                                foreach($results['instanceId'][$seriesId] as $instanceId){
                                                    $node2++;
                                            ?>
                                                    <div id="objTreeMenu_1_node_1_<?php echo $node0; ?>_<?php echo $node1; ?>_<?php echo $node2; ?>" style="display: inline;" class="treeMenuDefault">
                                                        <nobr>
                                                            <img src="<?php echo $GLOBALS['webroot'];?>/images/line.gif" width="20" height="20" align="top">
                                                            <img src="<?php echo $GLOBALS['webroot'];?>/images/linebottom.gif" width="20" height="20" align="top">
                                                            <img src="<?php echo $GLOBALS['webroot'];?>/images/branch.gif" width="20" height="20" align="top" border="0" name="img_objTreeMenu_1_node_1_<?php echo $node0; ?>_<?php echo $node1; ?>_<?php echo $node2; ?>">
                                                            <img src="<?php echo $GLOBALS['webroot'];?>/images/folder.gif" width="20" height="20" align="top" id="icon_objTreeMenu_1_node_1_<?php echo $node0; ?>_<?php echo $node1; ?>_<?php echo $node2; ?>">
                                                            <?php 
                                                                foreach($results['instanceResults'] as $key=>$value){
                                                                    if($key == $instanceId){
                                                                        echo '<a style="cursor:pointer;"><span id="'.$key.'" onclick="loadInstanceResult(this.id);">'.$value[0].'</span></a>'; 
                                                                    }
                                                                }
                                                            ?>                                                                
                                                        </nobr><br>
                                                    </div>
                                            <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </fieldset>
                    </div>			
                </div>	
                <div class="col-sm-9">
                    <div id="documents-actions">
                        <fieldset>				
                            <legend>Imaging Report</legend>
                            <div style="padding: 0 10px" class="result">
                                
                            </div>
                        </fieldset>			
                    </div>                    
                </div>                
            </div>
        </div><!--end of container div-->       
        <script language="javascript" type="text/javascript">
            
            objTreeMenu_1 = new TreeMenu("<?php echo $GLOBALS['webroot'];?>/images", "objTreeMenu_1", "_self", "treeMenuDefault", true, false);
            newNode = objTreeMenu_1.addItem(new TreeNode('Categories', 'folder.gif', '/dwellize-emr/dwellize_emr/controller.php?document&amp;upload&amp;patient_id=1&amp;parent_id=1&amp;', false, true, '', '', 'folder-expanded.gif'));
            <?php
                $node0 =0;
                foreach($results['studyId'] as $studyId){
                    $node0++;
            ?>
                    newNode_<?php echo $node0; ?> = newNode.addItem(new TreeNode('<?php foreach($results['studyResults'] as $key=>$value){if($key == $studyId){echo $value[0]."::".$value[1]; }}?>', 'folder.gif', '/dwellize-emr/dwellize_emr/controller.php?document&amp;upload&amp;patient_id=1&amp;parent_id=6&amp;', false, true, '', '', 'folder-expanded.gif'));
                    <?php
                        $node1 = 0;
                        foreach($results['seriesId'][$studyId] as $seriesId){
                            $node1++;
                    ?>
                            newNode_<?php echo $node0; ?>_<?php echo $node1; ?> = newNode_<?php echo $node0; ?>.addItem(new TreeNode('<?php foreach($results['seriesResults'] as $key=>$value){if($key == $seriesId){echo $value[0];}}?>', 'folder.gif', '/dwellize-emr/dwellize_emr/controller.php?document&amp;upload&amp;patient_id=1&amp;parent_id=7&amp;', false, true, '', '', 'folder-expanded.gif'));
                            <?php
                                $node2 = 0;
                                foreach($results['instanceId'][$seriesId] as $instanceId){
                                    $node2++;
                                ?>
                                    newNode_<?php echo $node0; ?>_<?php echo $node1; ?>_<?php echo $node2; ?> = newNode_<?php echo $node0; ?>_<?php echo $node1; ?>.addItem(new TreeNode('<?php foreach($results['instanceResults'] as $key=>$value){if($key == $instanceId){echo $value[0];}}?>', 'folder.gif', '/dwellize-emr/dwellize_emr/controller.php?document&amp;upload&amp;patient_id=1&amp;parent_id=21&amp;', false, true, '', '', 'folder-expanded.gif'));
                                <?php } ?>
                    <?php } ?>
            <?php } ?>
                      
            objTreeMenu_1.drawMenu();
            //objTreeMenu_1.writeOutput();
            objTreeMenu_1.resetBranches();                                               
            
            function loadStudyResult(id){
                $.ajax({ url: '<?php echo $web_root; ?>/interface/forms/radiology_image_report/ajax.php',
                    type: 'POST',
                    data: { studyResult:  <?php echo json_encode($results["studyResults"])?>,
                            studyId: id
                          },
                    success: function(returnData){
                        $( ".result" ).html(returnData);                        
                    },
                   error: function (XMLHttpRequest, textStatus, errorThrown){
                       alert(XMLHttpRequest.responseText);                       
                    }
                });                               
            } 
            
            function loadSeriesResult(id){
                $.ajax({ url: '<?php echo $web_root; ?>/interface/forms/radiology_image_report/ajax.php',
                    type: 'POST',
                    data: { seriesResult:  <?php echo json_encode($results["seriesResults"])?>,
                            seriesId: id
                          },
                    success: function(returnData){
                        $( ".result" ).html(returnData);                        
                    },
                   error: function (XMLHttpRequest, textStatus, errorThrown){
                       alert(XMLHttpRequest.responseText);                       
                    }
                });                               
            }
            
            function loadInstanceResult(id){
                $.ajax({ url: '<?php echo $web_root; ?>/interface/forms/radiology_image_report/ajax.php',
                    type: 'POST',
                    data: { instanceResult:  <?php echo json_encode($results["instanceResults"])?>,
                            instanceId: id
                          },
                    success: function(returnData){
                        $( ".result" ).html(returnData);                        
                    },
                   error: function (XMLHttpRequest, textStatus, errorThrown){
                       alert(XMLHttpRequest.responseText);                       
                    }
                });                               
            }                       
        </script>        
    </body>
</html>